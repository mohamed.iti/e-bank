export class GlobalConstants {

  public static apiURL: string = "https://localhost:8000/";



  public static token: string = "";
  public static appUser: string = "";
  public static appUserRole: string = "";
  public static utilisateurId: number = 1;
  public static ERROR_AUTHENTICATION_MESSAGE: string ="Login ou mot de passe incorrect";
  public static SUCCESS_AUTHENTICATION_MESSAGE :string ="Vous avez bien s'authentifie";
  public static ERROR_AUTHORIZATION_MESSAGE :string ="Action besoin d'autorisation , contacter l'adminstrateur du système";
  public static ERROR_AUTHENTICATION_CODE :number = 403;
  public static SUCCESS_AUTHENTICATION_CODE :number = 200;
  public static ERROR_AUTHENTICATION_STATUS:boolean =false;
  public static JWT_TOKEN_EXPIRATION_DURATION: number =-1;
  public static TOKEN_EXPIRATION_DATE:Date=new Date();
  public static WARNING_AUTHENTICATION_MESSAGE: string ="Session invalide, veuillez s’authentifier ";
  public static EXPIRED_AUTHENTICATION_STATUS:boolean =false;


}
