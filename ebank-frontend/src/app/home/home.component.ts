import { Component } from '@angular/core';
import {GlobalConstants} from "../globals/global-constants";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  protected readonly GlobalConstants = GlobalConstants;
}
