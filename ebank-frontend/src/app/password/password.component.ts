import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {CompteService} from "../services/compte.service";
import {PasswordService} from "../services/password.service";

@Component({
  selector: 'app-password',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit{

  ngOnInit() {

    this.initCompteForm();

  }

  constructor(private httpClient:HttpClient, private formBuilder :FormBuilder,
              private passwordService:PasswordService){

  }
  passwordFormGroup! : FormGroup;
//Formulaire attributs titres
  passwordFieldTitle:string ="password  ";
  IdentiteFieldTitle: string ="numeroIdentite ";

  initCompteForm(){
    this.passwordFormGroup= this.formBuilder.group(
      {
        //userIdClient:this.formBuilder.control("",Validators.required),
        password: this.formBuilder.control("", Validators.required),
        numeroIdentite: this.formBuilder.control("", Validators.required),
      }
    )

  }
  nouveaupassword(nouveaupassword:any){

//Recupperation données client du formulaire
    let passwordFormData = JSON.stringify(nouveaupassword.value);
    console.log(passwordFormData);
    let jsonObject = JSON.parse(passwordFormData);

//Appel service client
    this.passwordService.creerNouveaupassword(jsonObject).subscribe(
      {
        next :reponse => {
          console.log("password bien changé",reponse);
          //this.chargerListClients();
        }, error :err =>    {
          console.log("Error password changement",err)
        }
      }
    )
  }

}
