export interface  Operation {
  id: number
  typeOperation: string
  intituleDto: IntituleDto
  montant: number
  dateRealisation: string
  rib: string
}

export interface IntituleDto {
  id: number
  nom: string
  prenom: string
  login: string
  password: string
  passwordValidation: any
  dateAnniversaire: string
  adresseMail: string
  adressePostale: string
  numeroIdentite: string
  compteDtoLists: any[]
  listUtilisateurProfileDto: ListUtilisateurProfileDto[]
}

export interface ListUtilisateurProfileDto {
  crerationDate: any
  utilisateurProfileKey: UtilisateurProfileKey
}

export interface UtilisateurProfileKey {
  utilisateurId: number
  profileId: number
}
