import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GlobalConstants} from "../globals/global-constants";

@Component({
  selector: 'app-login', templateUrl: './login.component.html', styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isAuthenticatedError: boolean = false;
  userLoginFormGroup!: FormGroup;

  constructor(private authenticationService: AuthenticationService, private router: Router, private formBuilder: FormBuilder) {

  }
  private isTokenExpired(token: string) {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return expiry * 1000 > Date.now();
  }

  ngOnInit(): void {
  if(this.authenticationService.accessToken !=null){
    if (this.isTokenExpired(this.authenticationService.accessToken)) {
      // call logout method/dispatch logout event
      this.router.navigate(['/home']).then(r => true);
      console.log("Session invalide, veuillez s’authentifier ")
    } else {
      // token is valid: send requests...
    }
  }
    this.userLoginFormGroup = this.formBuilder.group({
      login: this.formBuilder.control("", Validators.required),
      password: this.formBuilder.control("", Validators.required),
    })
  }


  login(userLogin: any) {
//Recuperation données login  du formulaire
    let loginFormFormData = JSON.stringify(userLogin.value);
    let jsonUserLoginObject = JSON.parse(loginFormFormData);

//Appel service login
    this.authenticationService.login(jsonUserLoginObject).subscribe({

      next: response => {
        if (response != null) {
          this.authenticationService.loadDataUser(response);
          this.router.navigate(['/home']).then(r => true);
        } else {
          this.isAuthenticatedError = true;
          setTimeout(() => {
            let alertAuthenticationError = <HTMLElement>document.getElementById('error-authentication-alert');
            alertAuthenticationError.style.display = 'none';

            this.isAuthenticatedError = false;
          }, 3000);

        }
      }
    });

  }

  protected readonly GlobalConstants = GlobalConstants;
}




