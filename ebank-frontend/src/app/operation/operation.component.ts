import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {VirementService} from "../services/virement.service";
import {GlobalConstants} from "../globals/global-constants";
import {CompteService} from "../services/compte.service";


@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {


  //0-Declare form attribute controllers and init it by custom method
  operationVirementFormGroup!: FormGroup;
  listCrediteurRib!: Array<string>;


  //1-Declare Constructor and inject services(http or others)
  public constructor(private compteService: CompteService,private formBuilder: FormBuilder, private operationVirementService: VirementService) {
    console.log("Call construct .....")
  }

  //2-Implement ngOnInit methodn
  ngOnInit(): void {
    console.log("Init Virement Operation Component...");
    this.initClientForm();
    console.log("Init RIB list  Operation Component...");
    this.initClientListRib();
    console.log("Init OperationPagination list  Operation Component...");

  }
initClientListRib(){
  this.compteService.listRibParUtilisateurId().subscribe({

    next: response => {
      if (response != null) {

        console.log("Chargement list rib ok :",response);
        let listRibResponseApiData = JSON.stringify(response.valueOf());
        let jsonListRibObject = JSON.parse(listRibResponseApiData);
        console.log("List of rib json object = ",jsonListRibObject);
        //Fill rib list array
        this.listCrediteurRib=jsonListRibObject;
        console.log("listCrediteurRib = ",this.listCrediteurRib);
      } else {

        console.log("Error Chargement list rib response ",response);
      }
    },error:err =>{

      console.log("Error Chargement list rib = ",err);

    }
  });
}
  initClientForm() {
    this.operationVirementFormGroup = this.formBuilder.group({
      ribCrediteur: this.formBuilder.control("", Validators.required),
      montant: this.formBuilder.control("", Validators.required),
      ribDestinateur: this.formBuilder.control("", Validators.required),
      motif: this.formBuilder.control("", Validators.required),
      utilisateurCrediteurId: this.formBuilder.control("", Validators.required),
      typeOperation: this.formBuilder.control("", Validators.required),
    })

  }


  //3-Call custom service API Http (from service)


  nouveauOperationVirement(operationFormObject: any) {
    console.log("Call nouveauOperationVirement method ....");
    operationFormObject.value.utilisateurCrediteurId=GlobalConstants.utilisateurId;
    operationFormObject.value.typeOperation="VIRMENT";
//Recuperation données operation virement  du formulaire
    let operationFormFormData = JSON.stringify(operationFormObject.value);
    let jsonOperationObjectObject = JSON.parse(operationFormFormData);
    console.log("jsonOperationObjectObject == > ",jsonOperationObjectObject);

//Appel service virement
    this.operationVirementService.createOperation(jsonOperationObjectObject).subscribe({

      next: response => {
        if (response != null) {

          console.log("Bonne creation operation :")
        } else {

          console.log("Error creation virment ");
        }
      },error:err =>{

        console.log("Error creation virment = ",err);

      }
    });

  }




}
