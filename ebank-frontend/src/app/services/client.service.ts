import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";

const baseUrl: string = "http://localhost:3000/clients/";
const headerDict = {

  'Access-Control-Allow-Origin': 'Allow',
}
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private httpClient: HttpClient) {
  }


  creerNouveauClient(client: any) {
    console.log("Service creer client calling");

    return this.httpClient.post("http://localhost:8000/ebank/nouveauClient", client);
  }
  /*
  afficherListClients():Observable<any>{

    return this.httpClient.get("http://localhost:8000/ebank/clients");
  } */
}
