import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(private httpClient: HttpClient) { }
  creerNouveaupassword(newPasswordObject: any) {
    console.log("Service creerNouveaupassword calling");
    return this.httpClient.put("http://localhost:8000/ebank/changerpassword", newPasswordObject);
  }
}
