import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {GlobalConstants} from "../globals/global-constant";
export interface CompteDto {
  rib: string;
  solde: number | null;
}
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private apiUrl = 'http://localhost:8000/ebank/listRibSolde';

  constructor(private http: HttpClient) { }
  getListRibSolde(utilisateurId: number): Observable<CompteDto[]> {
    const params = new HttpParams().set('utilisateurId', utilisateurId.toString());
    return this.http.get<CompteDto[]>(this.apiUrl, { params });
  }

  /*getData(): Observable<any> {
    const paramValue = 1; // Affectation de la valeur 1 à paramValue
    const params = new HttpParams().set('utilisateurId', paramValue.toString());
    // @ts-ignore
    return this.http.get<any>('http://localhost:8000/ebank/listRibSolde',{params});

  }  */
}
