import {HttpClient, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map, Observable, pipe} from 'rxjs';
import {jwtDecode} from "jwt-decode";
import {GlobalConstants} from "../globals/global-constants";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated: boolean = false;
  username: any;
  roles: any;
  accessToken!: string;


  constructor(private http: HttpClient) {
  }


  login(userLogin: any): Observable<any> {
    console.log("Service login calling");

    return this.http.post("http://localhost:8000/login", userLogin);
//    return this.http.post("http://localhost:8000/login", userLogin,{observe:'response'});
  }


  loadDataUser(data: any) {
    this.isAuthenticated = true;
    this.accessToken = data['access-token'];
    //Parse token in order to extract user information, username and roles
    //using jwt-decode library
    let decodedJwt: any = jwtDecode(this.accessToken);
    this.username = decodedJwt.sub;
    this.roles = decodedJwt.roles;
    console.log("access-token : " + this.accessToken);
    console.log("username : " + this.username);
    console.log("roles : " + this.roles);
    GlobalConstants.token = this.accessToken;
    GlobalConstants.appUser = this.username;
    GlobalConstants.appUserRole = this.roles;
    /*const expirationJwtTokenOnTimestamp: string = decodedJwt.exp;
    console.log("expirationJwtTokenOnTimestamp Token = ",expirationJwtTokenOnTimestamp);*/
    const expirationJwtTokenOnTimestamp = decodedJwt.exp;
    const dateJwtToken = new Date(expirationJwtTokenOnTimestamp * 1000);
    var differenceValue =(dateJwtToken.getTime() - new Date().getTime()) / 1000;
    differenceValue /= 60;
    console.log("    Math.abs(Math.round(differenceValue)) = ",    Math.abs(Math.round(differenceValue)));

    GlobalConstants.JWT_TOKEN_EXPIRATION_DURATION=Math.abs(Math.round(differenceValue));
    GlobalConstants.TOKEN_EXPIRATION_DATE=dateJwtToken;

  }





}
