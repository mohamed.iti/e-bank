import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {map, Observable} from "rxjs";

export interface OperationDto {
  Date_operation:Date;
  Type_operation:string;
  Montant_operation:number | null;
  virement_en_votre_faveur:string;
  rib: string;

}
@Injectable({
  providedIn: 'root'
})
export class OperationService {

  private baseUrl = 'http://localhost:8000/ebank/operationsTopTen';

  constructor(private http: HttpClient) {}

  getOperations(pageNo: number, pageSize: number, sortBy: string, sortDirection: string, utilisateurId: number, rib?: string): Observable<any> {
    let params = new HttpParams()
      .set('pageNo', pageNo.toString())
      .set('pageSize', pageSize.toString())
      .set('sortBy', sortBy)
      .set('sortDirection', sortDirection)
      .set('utilisateurId', utilisateurId.toString());

    if (rib) {
      params = params.set('RIB', rib);
    }

    return this.http.get<any>(this.baseUrl, { params });
  }
}
