import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class VirementService {


  constructor(private http: HttpClient) {
  }


  createOperation(operationVirementObject: any): Observable<any> {
    console.log("Service operation Virement  calling");

    return this.http.post("http://localhost:8000/ebank/operation", operationVirementObject);
  }




}
