import { TestBed } from '@angular/core/testing';

import { HttpAppInterceptor } from './http-app.interceptor';

describe('HttpAppInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpAppInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HttpAppInterceptor = TestBed.inject(HttpAppInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
