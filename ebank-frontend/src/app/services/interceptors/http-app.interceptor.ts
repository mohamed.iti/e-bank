import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthenticationService} from "../authentication.service";

@Injectable()
export class HttpAppInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
  if(!request.url.includes("/login")) {
    let newRequest = request.clone({

      headers: request.headers.set('Authorization', this.authenticationService.accessToken)
    });

console.log("Total-Elements= ",newRequest.headers.get('Total-Elements'))
    return next.handle(newRequest);
  }else {
    return next.handle(request);

  }
}
}
