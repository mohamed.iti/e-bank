import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GlobalConstants} from "../globals/global-constants";

const httpHeaders: HttpHeaders = new HttpHeaders({
  Authorization: GlobalConstants.token
});
@Injectable({
  providedIn: 'root'
})
export class CompteService {

  constructor(private httpClient: HttpClient) { }
  creerNouveauCompte(compte: any) {
    console.log("Service creer compte calling");

    console.log("Token service compte = ",GlobalConstants.token)
    /*return this.httpClient.post("http://localhost:8000/ebank/nouveauCompte"
      , compte,
      { headers: httpHeaders });*/
    return this.httpClient.post("http://localhost:8000/ebank/nouveauCompte"
      , compte);

  }
  listRibParUtilisateurId() {
    console.log("Service get rib  compte calling");

    return this.httpClient.get(`http://localhost:8000/ebank/listComptes?utilisateurId=${GlobalConstants.utilisateurId}`);

  }
}
