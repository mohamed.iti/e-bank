import
{Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordService} from "../services/password.service";
import {CompteDto, DashboardService} from "../services/dashboard.service";
import {GlobalConstants} from "../globals/global-constant";
import {OperationDto, OperationService} from "../services/operation.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  //data: any;
  //dataForm!: FormGroup;
  data: CompteDto[] = [];
  utilisateurId: number = GlobalConstants.utilisateurid;
  operations: OperationDto[] = [];
  ribs: string[] = [];
  pageNo: number = 0;
  pageSize: number = 10;
  sortBy: string = 'dateRealisation';
  sortDirection: string = 'DESC';
  totalItems: number = 0;
  selectedRib: string | null = null;
  totalPages: number = 0;
  navigationElementArray: number[] = [];
  currentPageOperations: OperationDto[] = [];
  constructor(private httpClient:HttpClient, private formBuilder :FormBuilder,
              private dashboardService:DashboardService,private operationService: OperationService){

  }

  ngOnInit(): void {
    /*this.dataForm = this.formBuilder.group({
      rib: [], // Initialisez les champs du formulaire avec des valeurs par défaut
      solde: [],

    }); */
    //appel methode backend
    this.loadData();
    // this.fetchOperations();
    this.loadOperations();


  }

  loadData(): void{
    this.dashboardService.getListRibSolde(this.utilisateurId).subscribe({
      next: (data: CompteDto[]) => {
        this.data = data;
      },
      error: (error) => {
        console.error('There was an error!', error);
      },
      complete: () => {
        console.log('Request complete');
      }
    });
  }



    fetchOperations(): void {
      this.operationService.getOperations(this.pageNo, this.pageSize, this.sortBy, this.sortDirection, this.utilisateurId)
        .subscribe({
          next: (data: OperationDto[]) => {
            this.operations = data.map(operation => ({
              ...operation,
              dateOperation: new Date(operation.Date_operation)
            }));
            console.log(this.operations);
          },
          error: (error) => {
            console.error('Error fetching operations', error);
          }
        });
    }
  loadOperations(): void {
    const rib = this.selectedRib === null ? undefined : this.selectedRib;
    this.operationService.getOperations(this.pageNo, this.pageSize, this.sortBy, this.sortDirection, this.utilisateurId, rib)
      .subscribe({
        next: (data: OperationDto[]) => {
          this.operations = data;
          this.totalPages = Math.ceil(this.operations.length / this.pageSize);
          this.updateCurrentPageOperations();
        },
        error: (error) => {
          console.error('Error fetching operations', error);
          this.operations = [];
          this.totalPages = 1;
        }
      });
  }

  updateCurrentPageOperations(): void {
    const startIndex = this.pageNo * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.currentPageOperations = this.operations.slice(startIndex, endIndex);
  }

  onPageChange(pageNo: number): void {
    if (pageNo >= 0 && pageNo < this.totalPages) {
      this.pageNo = pageNo;
      this.updateCurrentPageOperations();
    }
  }

  onRIBChange(): void {
    this.pageNo = 0; // Reset to first page
    this.loadOperations();
  }

  toggleSortDirection(): void {
    this.sortDirection = this.sortDirection === 'ASC' ? 'DESC' : 'ASC';
    this.loadOperations();
  }
}
