import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClientService} from "../services/client.service";
import  {CompteService} from "../services/compte.service";

@Component({
  selector: 'app-comptes',
  templateUrl: './comptes.component.html',
  styleUrls: ['./comptes.component.css']
})
export class ComptesComponent implements OnInit{


  ngOnInit() {

    this.initCompteForm();
    /* this.chargerlistcompte(); */

  }
 /* listeComptes :Array<any>=[ ];

compteFieldsTitles : any ={listComptesFieldTitle:"liste Comptes",compteIDFieldTitle:"Compte ID",soldeFieldTitle:"Solde ",
ribFieldTitle:"RIB",userIdFieldTitle :"User ID" ,statusFieldTitle :"Status"};
*/

  constructor(private httpClient:HttpClient, private formBuilder :FormBuilder,
              private compteService:CompteService){

  }
  compteFormGroup! : FormGroup;
//Formulaire attributs titres
  RIBFieldTitle:string ="rib  ";
  IdentiteFieldTitle: string ="numeroIdentite ";

  initCompteForm(){
    this.compteFormGroup= this.formBuilder.group(
      {
        //userIdClient:this.formBuilder.control("",Validators.required),
        rib: this.formBuilder.control("", Validators.required),
        numeroidentite: this.formBuilder.control("", Validators.required),
      }
    )

  }

  nouveauCompte(nouveauCompte:any){

//Recupperation données client du formulaire
    let compteFormData = JSON.stringify(nouveauCompte.value);
    console.log(compteFormData);
    let jsonObject = JSON.parse(compteFormData);

//Appel service client
    this.compteService.creerNouveauCompte(jsonObject).subscribe(
      {
        next :reponse => {
          console.log("compte bien crée",reponse);
          //this.chargerListClients();
        }, error :err =>    {
          console.log("Error compte création",err)
        }
      }
    )
  }
  /*
 chargerlistcompte(){
    this.httpClient.get<any>(`http://localhost:3000/comptes`).subscribe(
      {
        next: reponse => this.listeComptes = reponse
        , error: err => {
          console.log("Error chargement liste Comptes")
        }
      }


    )

} */
}
