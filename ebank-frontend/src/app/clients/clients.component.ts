import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent     implements OnInit {
  //Init data clients on load
  ngOnInit() {

    this.initClientForm();
   /* this.chargerListClients(); */

  }
//Use constructor to inject dependencies service on componants

  constructor(private httpClient:HttpClient
    ,private formBuilder :FormBuilder,
              private clientService:ClientService){


  }
  //Form fields controllers
  clientFormGroup! : FormGroup;
//Formulaire attributs titres
userIdFieldTitle: any="User ID";
nomFieldTitle:string ="nom  ";
prenomFieldTitle: string ="prénom ";
loginFieldTitle:string ="login  ";
passwordFieldTitle:string ="password ";
passwordValidationFieldTitle:string ="passwordValidation  ";
dateAnniversaireFieldTitle:string = "dateAnniversaire  ";
adresseMmailFieldTitle:string = "adresseMail  ";
adressePostaleFieldTitle: string ="adressePostale";
numeroIdentiteFieldTitle: string ="numeroIdentite ";

listProfiles: any ={ listProfilesFieldTitle:"Liste Profiles " ,idFieldTitle:"Profile ID ", profileNameFieldTitle:"Profile Name "};
/* listOperations :any = {listOperationFieldTitle:"Liste Opérations",operationIDFieldTitle:"Opération ID  ",operationDateFieldTitle:"Date Opération ",typeOperationFieldTitle:"Type Opération :"};
listComptes : any ={listComptesFieldTitle:"liste Comptes",compteIDFieldTitle:"Compte ID",soldeFieldTitle:"Solde ",ribFieldTitle:"RIB"};

//Les données liste clients provient de l'api
listeClients :Array<any>=[]

*/
//Initialiser les controlleurs formulaire avec des regles de gestion par Validators, et FormBuilder

initClientForm(){
this.clientFormGroup= this.formBuilder.group(
  {
    //userIdClient:this.formBuilder.control("",Validators.required),
nom:this.formBuilder.control("",Validators.required),
prenom:this.formBuilder.control("",Validators.required),
//loginClient: this.formBuilder.control("",Validators.required),
//passwordClient: this.formBuilder.control("",Validators.required),
//passwordValidationClient : this.formBuilder.control("",Validators.required),
profileClient :this.formBuilder.control("",Validators.required),
    dateAnniversaire: this.formBuilder.control("",Validators.required),
    adresseMail : this.formBuilder.control("",Validators.required),
    adressePostale:  this.formBuilder.control("",Validators.required),
    numeroIdentite:this.formBuilder.control("",Validators.required),

  }
)

}
//nomClient :string ="";
  //Création nouveau client
nouveauClient(nouveauClient:any){
//Recupperation données client du formulaire
  let clientFormData = JSON.stringify(nouveauClient.value);
  console.log(clientFormData);
  let jsonObject = JSON.parse(clientFormData);
//Appel service client
  this.clientService.creerNouveauClient(jsonObject).subscribe(
    {
      next :reponse => {
        console.log("client bien créer",reponse);
        //this.chargerListClients();
      }, error :err =>    {
        console.log("Error client création",err)
      }
    }
  )
}
/*
chargerListClients(){
  this.httpClient.get<any>(`http://localhost:3000/clients`).subscribe(

  {
    next :reponse => this.listeClients=reponse
    ,error :err =>{
      console.log("Error chargement liste clients")
    }
  }



  )
} */

}
