import { NgModule } from '@angular/core';
import {CanActivateFn, RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ClientsComponent } from './clients/clients.component';
import { ComptesComponent } from './comptes/comptes.component';
import {PasswordComponent} from "./password/password.component";
import {authenticationGuard} from "./guards/authentication.guard";
import {OperationComponent} from "./operation/operation.component";
import {DashboardComponent} from "./dashboard/dashboard.component";

const routes: Routes = [
  {path:'home',component :HomeComponent

        ,children :[
          {path:'clients',component :ClientsComponent}
          ,
          {path:'comptes',component :ComptesComponent}
          ,
          {path:'password',component :PasswordComponent,canActivate:[authenticationGuard]},
           {path:'operation',component :OperationComponent,canActivate:[authenticationGuard]},
      {path:'dashboard',component :DashboardComponent,canActivate:[authenticationGuard]}
        ]
}
  ,
  {path :'login' , component : LoginComponent,canActivate:[authenticationGuard]}
  ,
  {path:'',component :LoginComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
