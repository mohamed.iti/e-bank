import {CanActivateFn, Router} from '@angular/router';
import {AuthenticationService} from "../services/authentication.service";
import {inject, Inject} from "@angular/core";
import {GlobalConstants} from "../globals/global-constants";
export const authenticationGuard: CanActivateFn = (route, state) => {
  const token :string=GlobalConstants.token;
  const router=inject(Router);

  if(token) {
    console.log("Globals token expiration from authenticationGuard = ",GlobalConstants.JWT_TOKEN_EXPIRATION_DURATION);
    console.log("TOKEN_EXPIRATION_DATE from authenticationGuard = ",GlobalConstants.TOKEN_EXPIRATION_DATE);
    let differenceValue =(GlobalConstants.TOKEN_EXPIRATION_DATE.getTime() - new Date().getTime()) / 1000;
    differenceValue /= 60;
    console.log("  differenceValue from authenticationGuard  Math.abs(Math.round(differenceValue)) = ",((GlobalConstants.TOKEN_EXPIRATION_DATE.getTime() - new Date().getTime())/(1000*60)));
    const value:number=((GlobalConstants.TOKEN_EXPIRATION_DATE.getTime() - new Date().getTime())/(1000*60));
    console.log("  value from authenticationGuard  = ",Math.round(value));

    if( Math.round(value) <= 0)
    { GlobalConstants.EXPIRED_AUTHENTICATION_STATUS=true;
      //router.navigate(['login']).then(r => false);
      console.log("Session invalide, veuillez s’authentifier ")
    }
    return true;
  }
else
    router.navigate(['login']).then(r => false);
  return false;
};
