package com.formations.ebank.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Aspect
@Component
public class LogAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Before("execution(public * com.formations.ebank.services.*Service.changePasswordByPasswordEncoder(..))")
    public void logBeforeChangePassword() {
        logger.info("La méthode changePasswordByPasswordEncoder est sur le point d'être exécutée.");
    }
    @Before("execution(public * com.formations.ebank.controllers.*.getListRibSolde(..))")
    public void logBeforeGetListRibSolde() {
        logger.info("La méthode getListRibSolde est sur le point d'être exécutée.");
    }

}
