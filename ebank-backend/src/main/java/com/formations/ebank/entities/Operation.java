package com.formations.ebank.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.formations.ebank.enums.EnumTypeOperation;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.io.Serializable;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Operation  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Enumerated(EnumType.ORDINAL)
    @JdbcTypeCode(SqlTypes.ENUM)
    private EnumTypeOperation typeOperation;
    private Double montant;
    private Date dateRealisation;
    private  String motif;
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "utilisateur_id",referencedColumnName = "id" )
    @JsonIgnore
    private Utilisateur utilisateur;
    private String rib;

}
