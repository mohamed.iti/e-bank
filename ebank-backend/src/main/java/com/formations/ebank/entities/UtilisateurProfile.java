package com.formations.ebank.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurProfile implements Serializable {


    private Date crerationDate;

    @EmbeddedId
    private UtilisateurProfileKey utilisateurProfileKey;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @MapsId("utilisateur_id")
    @JoinColumn(name = "utilisateur_id",referencedColumnName = "id")
    @JsonIgnore
    @ToString.Exclude
    private Utilisateur utilisateur;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("profile_id")
    @JoinColumn(name = "profile_id",referencedColumnName = "id")
    @JsonIgnore
    @ToString.Exclude
    private Profile profile;


}
