package com.formations.ebank.services.impl;

import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Compte;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.exceptions.BusinessException;
import com.formations.ebank.exceptions.BussinessException;
import com.formations.ebank.repositories.CompteJpaRepo;
import com.formations.ebank.services.ICompteService;
import com.formations.ebank.utils.CustomModelMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class ICompteServiceImpl implements ICompteService {

    private  final CompteJpaRepo compteJpaRepo;
private final CustomModelMapper customModelMapper;

    @Override
    public CompteDto nouveauCompte(CompteDto compteDto) {

        try {
              Compte compteBo=  this.customModelMapper.modelMapper().map(compteDto,Compte.class);
            compteBo.setDerniereDateModification(new Date());

            var savedCompteBo =this.compteJpaRepo.save(compteBo);

            return customModelMapper.modelMapper().map(savedCompteBo,CompteDto.class);
        }catch (Exception e){

            return null;
        }

    }

    @Override
    public List<CompteDto> listeCompte() {
        return null;
    }

    /*
        @Override
        public List<CompteDto> listeCompte() {
            return null;
        }

        @Override
        public CompteDto listeCompteByUtilisateur(Integer utilisateurId) {
            var listecompte=this.customModelMapper.modelMapper().map(compteJpaRepo.findById(utilisateurId).
                    stream().collect(Collectors.toList()),CompteDto.class);
            return listecompte;
        }
        */
    @Override
   public List<CompteDto> listeCompteByUtilisateur(UtilisateurDto utilisateurDto) {

       return    compteJpaRepo.findByUtilisateur(customModelMapper
                  .modelMapper().
                  map(utilisateurDto,
                          Utilisateur.class)).stream().map(compte -> customModelMapper.modelMapper()
                  .map(compte,CompteDto.class)).collect(Collectors.toList());
    }



    @Override
    public void attacheCompteToUtilisateur(UtilisateurDto utilisateurDto, CompteDto compteDto) {

        try {

        }catch (Exception e){

        }
    }

    @Override
    public CompteDto chercherCompteParRib(String rib) {

        return customModelMapper.modelMapper()
                .map(this.compteJpaRepo.findCompteByRib(rib).get(),CompteDto.class);
    }

    @Override
    public CompteDto checherCompteRecementMouvemente(UtilisateurDto utilisateurDto) {

        List<Compte> compteListParUtilisateur=compteJpaRepo.findByUtilisateur(customModelMapper
                .modelMapper().map(utilisateurDto,Utilisateur.class));
        Optional<Compte> compteBoLastUpdated=compteListParUtilisateur.stream()
                .max(Comparator.comparing(Compte::getDerniereDateModification));
        return customModelMapper.modelMapper().map(compteBoLastUpdated.get(),CompteDto.class);
    }

    @Override
    public List<CompteDto> listeCompteByUtilisateurId(Integer utilisateurId) {
        return    compteJpaRepo.findByUtilisateurId(utilisateurId).stream().map(compte -> customModelMapper.modelMapper()
                .map(compte,CompteDto.class)).collect(Collectors.toList());
    }



}
