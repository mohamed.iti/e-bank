package com.formations.ebank.services.impl;

import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.OperationDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Operation;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.enums.EnumStatusCompte;
import com.formations.ebank.enums.EnumTypeOperation;
import com.formations.ebank.exceptions.CompteException;
import com.formations.ebank.exceptions.VirmentOperationException;
import com.formations.ebank.repositories.OperationJpaRepo;
import com.formations.ebank.services.ICompteService;
import com.formations.ebank.services.IOperationService;
import com.formations.ebank.utils.CustomModelMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class IOperationServiceImpl implements IOperationService {


    private final OperationJpaRepo operationJpaRepo;
    private final ICompteService iCompteService;
    private final CustomModelMapper customModelMapper;

    @Override
    public void nouveauOperation(String ribDestinatire,
                                 String ribCrediteur,
                                 Double montant,
                                 EnumTypeOperation typeOperation,
                                 UtilisateurDto utilisateurDto,
                                 String motifOperation) throws VirmentOperationException,
            CompteException{
        Operation operation = new Operation();
        CompteDto compteDto = null;
        switch (typeOperation) {
            case DEBIT -> {
                operation.setTypeOperation(typeOperation);
                compteDto = this.debitOperation(montant, ribDestinatire);
                operation.setMotif(motifOperation);
                operation.setMontant(montant);
                operation.setUtilisateur(customModelMapper.modelMapper().map(utilisateurDto, Utilisateur.class));
                operation.setDateRealisation(new Date());
                operation.setRib(ribDestinatire);

                iCompteService.nouveauCompte(compteDto);
                this.operationJpaRepo.save(operation);

            }
            case CREDIT -> {
                operation.setTypeOperation(typeOperation);
                compteDto = this.creditOperation(montant, ribCrediteur);
                operation.setMotif(motifOperation);
                operation.setMontant(montant);
                operation.setUtilisateur(customModelMapper.modelMapper().map(utilisateurDto, Utilisateur.class));
                operation.setDateRealisation(new Date());
                operation.setRib(ribCrediteur);

                iCompteService.nouveauCompte(compteDto);
                this.operationJpaRepo.save(operation);

            }
            case VIRMENT -> {
                if (ribDestinatire != null) {
                    //check balance uppermore than montant source
                    CompteDto compteDtoCrediteur = iCompteService.chercherCompteParRib(ribCrediteur);
                    if (compteDtoCrediteur != null  &&
                            !(compteDtoCrediteur.getStatusCompte()
                                    .equals(EnumStatusCompte.CLOUTURE) ||
                                    compteDtoCrediteur.getStatusCompte()
                                            .equals(EnumStatusCompte.BLOQUE) )){
                        Double montantCrediteur = compteDtoCrediteur.getSolde();
                        if (montantCrediteur >= montant) {
                            //Crediteur
                            Operation operationCreditVirement = new Operation();
                            operationCreditVirement.setTypeOperation(typeOperation);
                           CompteDto compteDtoCredit=  this.creditOperation(montant, ribCrediteur);
                            operationCreditVirement.setMotif(motifOperation);
                            operationCreditVirement.setMontant(montant);
                            operationCreditVirement.setUtilisateur(customModelMapper.
                                    modelMapper().map(utilisateurDto, Utilisateur.class));
                            operationCreditVirement.setDateRealisation(new Date());
                            iCompteService.nouveauCompte(compteDtoCredit);
                            operationCreditVirement.setRib(ribCrediteur);
                            this.operationJpaRepo.save(operationCreditVirement);
                            //Debiteur
                            Operation operationDebitVirement = new Operation();
                            operationDebitVirement.setTypeOperation(typeOperation);
                            CompteDto compteDtoDebitVirment = this.debitOperation(montant, ribDestinatire);
                            UtilisateurDto utilisateurDtoDebiteurVirement=compteDtoDebitVirment.getUtilisateurDto();
                            operationDebitVirement.setMotif(motifOperation);
                            operationDebitVirement.setMontant(montant);
                            operationDebitVirement.setUtilisateur(customModelMapper.modelMapper().
                                    map(utilisateurDtoDebiteurVirement, Utilisateur.class));
                            operationDebitVirement.setDateRealisation(new Date());
                            iCompteService.nouveauCompte(compteDtoDebitVirment);
                            operationDebitVirement.setRib(ribDestinatire);
                            this.operationJpaRepo.save(operationDebitVirement);

                        } else
                            throw new CompteException("Montant insuffisante");
                    }else
                        throw new VirmentOperationException("Compte bloqué ou clotûré");
                }else
                    throw new CompteException("RIB non existe");

            }

        }

    }

    @Override
    public List<OperationDto> listeOperationParUtilisateur(UtilisateurDto utilisateurDto) {
        return null;
    }

    @Override
    public CompteDto debitOperation(Double montant, String rib) {
        CompteDto compteDtoToDebit = iCompteService.chercherCompteParRib(rib);
        compteDtoToDebit.setSolde(compteDtoToDebit.getSolde() + montant);


        return compteDtoToDebit;
    }

    @Override
    public CompteDto creditOperation(Double montant, String rib) {
        CompteDto compteDtoToCredit = iCompteService.chercherCompteParRib(rib);
        compteDtoToCredit.setSolde(compteDtoToCredit.getSolde() - montant);
        return compteDtoToCredit;
    }

    @Override
    public void nouveauVirement(String ribCrediteur, String ribDestinataire, String motif, Double montant, UtilisateurDto utilisateurDto) {
        //check balance upper than montant source
        CompteDto compteDtoCrediteur = iCompteService.chercherCompteParRib(ribCrediteur);
        if (compteDtoCrediteur != null) {
            Double montantCrediteur = compteDtoCrediteur.getSolde();
            if (montantCrediteur >= montant) {
            }
        }
        //Debiteur distinataire

        //Crediteur source
    }

    public Page<Operation> findAll(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);

        return this.operationJpaRepo.findAll(pageable);
    }

    @Override
    public Page<OperationDto> findAllByUtilisateur(UtilisateurDto utilisateurDto,int pageNo,
                                                   int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);

       Page<Operation>operationsBo=  this.operationJpaRepo.
                findAllByUtilisateur(customModelMapper.modelMapper()
                        .map(utilisateurDto,Utilisateur.class),pageable);
       Page<OperationDto>operationDtoPage=operationsBo.map(operation -> customModelMapper.modelMapper()
               .map(operation,OperationDto.class));

        return operationDtoPage;

    }

    @Override
    public Page<OperationDto> chercherTopTenOperationParRib(String rib, int pageNo,
                                                            int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);



        Page<Operation>operationsBo=  this.operationJpaRepo.
                findAllByRib(rib,pageable);

        Page<OperationDto>operationDtoPage=
                operationsBo.map(operation -> customModelMapper.modelMapper()
                .map(operation,OperationDto.class));
        //customModelMapper.modelMapper().validate();


        return operationDtoPage;


    }
    public List<OperationDto> chercherOperationsParRib(String rib, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, sort);
        Page<Operation> operationsBo = this.operationJpaRepo.findAllByRib(rib, pageable);

        List<OperationDto> operationDtoList = operationsBo.getContent().stream()
                .map(operation -> customModelMapper.modelMapper().map(operation, OperationDto.class))
                .collect(Collectors.toList());

        return operationDtoList;
    }

    public List <OperationDto> chercherOperationrecementMouvemente(UtilisateurDto utilisateurDto){
        List<Operation> operationListParUtilisateur=operationJpaRepo.findByUtilisateur(customModelMapper.
                modelMapper().map(utilisateurDto, Utilisateur.class));
        Optional<Operation> operationBOLastUpdated=operationListParUtilisateur.stream().max(Comparator.comparing(
                Operation::getDateRealisation));
       // return customModelMapper.modelMapper().map(operationBOLastUpdated.get(),OperationDto.class);
        return operationListParUtilisateur.stream().map(operation-> customModelMapper.modelMapper()
                .map(operation,OperationDto.class)).collect(Collectors.toList());
    }
}
