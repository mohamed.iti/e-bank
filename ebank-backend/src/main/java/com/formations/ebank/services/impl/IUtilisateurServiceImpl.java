package com.formations.ebank.services.impl;

import com.formations.ebank.aspect.Loggable;
import com.formations.ebank.dtos.ChangePasswordDTO;
import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.ProfileDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.*;
import com.formations.ebank.enums.EnumSecurity;
import com.formations.ebank.enums.EnumTypeProfile;
import com.formations.ebank.exceptions.BussinessException;
import com.formations.ebank.exceptions.UtilisateurCreationException;
import com.formations.ebank.exceptions.UtilisateurCredentialsException;
import com.formations.ebank.repositories.ProfileJpaRepo;
import com.formations.ebank.repositories.UtilisateurJpaRepo;
import com.formations.ebank.repositories.UtilisateurProfileJpaRepo;
import com.formations.ebank.security.SecurityManagerService;
import com.formations.ebank.services.IUtilisateurService;
import com.formations.ebank.utils.CustomModelMapper;
import jakarta.transaction.RollbackException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.TypeMap;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class IUtilisateurServiceImpl implements IUtilisateurService {

    private final UtilisateurJpaRepo utilisateurJpaRepo;
    private final SecurityManagerService securityManagerService;
    private final CustomModelMapper customModelMapper;
    private final UtilisateurProfileJpaRepo utilisateurProfileJpaRepo;
    private final ProfileJpaRepo profileJpaRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<UtilisateurDto> seConnecter(String login, String password) throws UtilisateurCredentialsException {
       /* log.info("Entred login : "+login+"; password : "+password);
        String decryptMotDPasse =securityManagerService
                .crypterMotDePasse(password,"");
        log.info("EncodedMotDPasse  password:  "+encodedMotDPasse);*/
        var optionalBoUtilisateurConnecte = utilisateurJpaRepo.findByLogin(login);
        if (optionalBoUtilisateurConnecte.isPresent()) {
            String decryptedPassword = securityManagerService.decrypterMotDePasse(optionalBoUtilisateurConnecte.get().getPassword(), EnumSecurity.SecretKey.toString());
            System.out.println("decryptedPassword = " + decryptedPassword);
            if (decryptedPassword != null && decryptedPassword.equals(password)) {
                Utilisateur utilisateurConnecte = optionalBoUtilisateurConnecte.get();
                log.info("UserDTOConnected : ", utilisateurConnecte);
                UtilisateurDto utilisateurDto = customModelMapper.modelMapper().map(utilisateurConnecte, UtilisateurDto.class);
                return Optional.of(utilisateurDto);
            } else throw new UtilisateurCredentialsException("Login ou mot de passe erronés ");
        } else return Optional.empty();


    }

    @Override
    public UtilisateurDto creerClient(UtilisateurDto client) throws UtilisateurCreationException {

        var utilisateurBo = customModelMapper.modelMapper().map(client, Utilisateur.class);

        utilisateurBo.setPassword(passwordEncoder.encode(utilisateurBo.getPassword()));

        UtilisateurDto utilisateurDto = customModelMapper.modelMapper().map(this.utilisateurJpaRepo.save(utilisateurBo), UtilisateurDto.class);
        if (utilisateurDto != null) {
            return utilisateurDto;
        } else throw new UtilisateurCreationException("Cannot create utilisateur");
    }


    @Override
  /*  public UtilisateurDto changePassword(UtilisateurDto utilisateurDto) throws BussinessException {
        try {
            Utilisateur utilisateurbo=  this.customModelMapper.modelMapper().map(utilisateurDto,Utilisateur.class);


            var savedUtilisateurBo =this.utilisateurJpaRepo.save(utilisateurbo);

            return customModelMapper.modelMapper().map(savedUtilisateurBo, UtilisateurDto.class);
        }catch (Exception e){

            return null;
        }
    } */

    public UtilisateurDto changePassword(ChangePasswordDTO changePasswordDTO)   {

       Utilisateur utilisateurBo=utilisateurJpaRepo.findByNumeroIdentite(changePasswordDTO.getNumeroIdentite()).get();
        utilisateurBo.setPassword(changePasswordDTO.getPassword());
        utilisateurBo.setPassword(securityManagerService.crypterMotDePasse(utilisateurBo.getPassword(), EnumSecurity.SecretKey.toString()));

        UtilisateurDto updatepasswordResponse = this.customModelMapper.modelMapper().map(utilisateurJpaRepo.save(utilisateurBo), UtilisateurDto.class);


       return updatepasswordResponse;
    }

    @Override
    public Boolean verifierAccesFonctionnaliteAuhtenticite(UtilisateurDto utilisateurDto) {
        return null;
    }

    @Override
    public Boolean verifierAccesFonctionnaliteParAuthorite(UtilisateurDto utilisateurDto) {
        return null;
    }

    @Override
    public void affecterProfile(Integer utilisateurId, ProfileDto profileDto) {
        Optional<Utilisateur> utilisateur = utilisateurJpaRepo.findById(utilisateurId);
        if (utilisateur.isPresent()) {
            Utilisateur utilisateurBo = utilisateur.get();


            utilisateurJpaRepo.save(utilisateurBo);
            //create key utilisateurProfileKey
            UtilisateurProfileKey utilisateurProfileKey = UtilisateurProfileKey.builder().profileId(profileDto.getId()).utilisateurId(utilisateurBo.getId()).build();
            var utilisateurProfile = UtilisateurProfile.builder().profile(customModelMapper.modelMapper().map(profileDto, Profile.class)).utilisateur(utilisateurBo).utilisateurProfileKey(utilisateurProfileKey).build();
            utilisateurProfileJpaRepo.save(utilisateurProfile);

        }
    }

    @Override
    public void affecterProfile(Integer utilisateurId, Integer profileId) {
        ProfileDto profileDto=customModelMapper.modelMapper().map(profileJpaRepo.findById(profileId).get(),ProfileDto.class);
        Optional<Utilisateur> utilisateur = utilisateurJpaRepo.findById(utilisateurId);
        if (utilisateur.isPresent()) {
            Utilisateur utilisateurBo = utilisateur.get();


            utilisateurJpaRepo.save(utilisateurBo);
            //create key utilisateurProfileKey
            UtilisateurProfileKey utilisateurProfileKey = UtilisateurProfileKey.builder().profileId(profileDto.getId()).utilisateurId(utilisateurBo.getId()).build();
            var utilisateurProfile = UtilisateurProfile.builder().profile(customModelMapper.modelMapper().map(profileDto, Profile.class)).utilisateur(utilisateurBo).utilisateurProfileKey(utilisateurProfileKey).build();
            utilisateurProfileJpaRepo.save(utilisateurProfile);

        }
    }

    @Override
    public String genereMotDePasseAuto() {
        try {

            return "";
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String genereLoginAuto() {
        return null;
    }

    @Override
    public UtilisateurDto chercherUtilisateurParId(Integer id) {
        Optional<Utilisateur> optionalUtilisateur = this.utilisateurJpaRepo.findById(id);
        if (optionalUtilisateur.isPresent()) {
            Utilisateur utilisateurBo = optionalUtilisateur.get();
            return customModelMapper.modelMapper().map(utilisateurBo, UtilisateurDto.class);
        }
        return null;
    }

    @Override
    public List<UtilisateurDto> listeClients() {

        List<Utilisateur> clients = new ArrayList<>();
        this.utilisateurJpaRepo.findAll().forEach(utilisateur -> {
            utilisateur.getListUtilisateurProfile().stream()
                    .filter(utilisateurProfile -> utilisateurProfile.getProfile()
                            .getTypeProfile().equals(EnumTypeProfile.ROLE_CLIENT))
                    .map(utilisateurProfile -> utilisateur).forEach(clients::add);
        });

        List<UtilisateurDto>utilisateurDtos=new ArrayList<>();


           clients.stream().map(c -> customModelMapper
                .modelMapper().map(c, UtilisateurDto.class)).forEach(utilisateurDtos::add);

           return utilisateurDtos;
    }

    @Override
    public List<Utilisateur> listeClientsDao() {
        List<Utilisateur> clients = new ArrayList<>();
        this.utilisateurJpaRepo.findAll().forEach(utilisateur -> {
            utilisateur.getListUtilisateurProfile().stream()
                    .filter(utilisateurProfile -> utilisateurProfile.getProfile()
                            .getTypeProfile().equals(EnumTypeProfile.ROLE_CLIENT))
                    .map(utilisateurProfile -> utilisateur).forEach(clients::add);
        });
    return clients;
    }

    @Override
    public UtilisateurDto chercherUtilisateurParNumeroIdentite(String numeroIdentite) {
        Optional<Utilisateur> optionalUtilisateur = this.utilisateurJpaRepo.findByNumeroIdentite(numeroIdentite);
        if (optionalUtilisateur.isPresent()) {
            Utilisateur utilisateurBo = optionalUtilisateur.get();
            return customModelMapper.modelMapper().map(utilisateurBo, UtilisateurDto.class);
        }
        return null;
    }

    @Loggable
    @Override
    public UtilisateurDto changePasswordByPasswordEncoder(ChangePasswordDTO changePasswordDTO) {
        Utilisateur utilisateurBo=utilisateurJpaRepo.findByNumeroIdentite(changePasswordDTO.getNumeroIdentite()).get();
        utilisateurBo.setPassword(changePasswordDTO.getPassword());
        utilisateurBo.setPassword(passwordEncoder.encode(utilisateurBo.getPassword()));
        UtilisateurDto updatepasswordResponse = this.customModelMapper.modelMapper().map(utilisateurJpaRepo.save(utilisateurBo), UtilisateurDto.class);


        return updatepasswordResponse;
    }


}
