package com.formations.ebank.services;

import com.formations.ebank.dtos.ChangePasswordDTO;
import com.formations.ebank.dtos.ProfileDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Profile;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.exceptions.UtilisateurCreationException;
import com.formations.ebank.exceptions.UtilisateurCredentialsException;

import java.util.List;
import java.util.Optional;

public interface IUtilisateurService {
    Optional<UtilisateurDto> seConnecter(String login, String password) throws UtilisateurCredentialsException;
    UtilisateurDto creerClient(UtilisateurDto client) throws UtilisateurCreationException;
    //UtilisateurDto changePassword(UtilisateurDto utilisateurDto);
    UtilisateurDto changePassword(ChangePasswordDTO changePasswordDTO);
    Boolean verifierAccesFonctionnaliteAuhtenticite(UtilisateurDto utilisateurDto);
    Boolean verifierAccesFonctionnaliteParAuthorite(UtilisateurDto utilisateurDto);
    void affecterProfile(Integer id, ProfileDto profileDto);
    void affecterProfile(Integer utilisateurId, Integer profileId);

    String genereMotDePasseAuto();
    String genereLoginAuto();
    UtilisateurDto chercherUtilisateurParId(Integer id);
    List<UtilisateurDto>listeClients();
    List<Utilisateur>listeClientsDao();
    UtilisateurDto chercherUtilisateurParNumeroIdentite(String numeroIdentite);
    UtilisateurDto changePasswordByPasswordEncoder(ChangePasswordDTO changePasswordDTO);


}
