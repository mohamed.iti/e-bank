package com.formations.ebank.controllers;


import com.formations.ebank.dtos.ProfileDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.exceptions.UtilisateurCreationException;
import com.formations.ebank.security.IMailSenderService;
import com.formations.ebank.security.SecurityManagerService;
import com.formations.ebank.services.IUtilisateurService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor

public class ClientRestController {

    private final IUtilisateurService iUtilisateurService;
    private final SecurityManagerService securityManagerService;
    private  final IMailSenderService iMailSenderService;
    //Create new customer(nouveau client)
    //Get list of customers (liste utilisateurs ayant type profile client)


    @GetMapping("clients")
    @PostAuthorize("hasRole('ROLE_ADMIN')")
    public List<UtilisateurDto> getListClients() {
        log.info("Lsite clients page");

        return this.iUtilisateurService.listeClients();
    }

    /**
     *     @apiNote
     *     To review dto comptelist and dtoOperationListe not show when call this method
     */@GetMapping("listClients")
    @PostAuthorize("hasRole('ROLE_AGENT_GUICHET')")
    public List<Utilisateur> getListClientsDao() {
        log.info("Liste clients DAO");

        return this.iUtilisateurService.listeClientsDao();
    }

   /* @SneakyThrows
    @PostMapping("/nouveauUtilisateur")
    @PostAuthorize("hasRole('ROLE_AGENT_GUICHET')")
    public ResponseEntity<UtilisateurDto> createCustomer(@RequestBody @Valid UtilisateurDto utilisateurDto) throws UtilisateurCreationException {
        return new ResponseEntity<>(iUtilisateurService.creerClient(utilisateurDto), HttpStatus.CREATED);
    }*/
    @PostMapping("/nouveauClient")
    @PostAuthorize("hasRole('ROLE_AGENT_GUICHET')")
    void creerNouveauClient(@RequestBody UtilisateurDto utilisateurDtoClient) throws UtilisateurCreationException {




        //Generate auto login and password
        String autoLogin=securityManagerService.generateAutoLogin(utilisateurDtoClient.getNom(),
                utilisateurDtoClient.getPrenom());
        String autoMotDePasse= securityManagerService.generateAutoPassword();
        utilisateurDtoClient.setPassword(autoMotDePasse);
        utilisateurDtoClient.setLogin(autoLogin);

       UtilisateurDto utilisateurDto= iUtilisateurService.creerClient(utilisateurDtoClient);
       iUtilisateurService.affecterProfile(utilisateurDto.getId(),3);
        //Send mail
        iMailSenderService.sendNewMail(utilisateurDto.getAdresseMail(),"Votre compte a été bien créé"
                ,"Login : "+autoLogin+" \n Password  :"+autoMotDePasse);
    }

    @PostMapping("/affectProfile")
    void affectProfileToUtilisateur(@RequestParam (name = "utilisateurId") Integer utilisateurId,
                                    @RequestParam (name = "profileId")Integer  profileId) {
        iUtilisateurService.affecterProfile(utilisateurId,profileId);
    }

}
