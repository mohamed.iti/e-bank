package com.formations.ebank.controllers;


import com.formations.ebank.dtos.CompteCreationDto;
import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Compte;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.enums.EnumStatusCompte;
import com.formations.ebank.services.ICompteService;
import com.formations.ebank.services.IUtilisateurService;
import com.formations.ebank.utils.CustomModelMapper;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor
public class CompteRestController {

    private final ICompteService iCompteService;
    private final IUtilisateurService iUtilisateurService;
    private final CustomModelMapper customModelMapper;



   /* @PostMapping("nouveauCompte")
    public ResponseEntity<String> creerNouveauCompte(@RequestParam(name = "numeroIdentite") String numeroIdentite,
                                             @RequestBody CompteDto compteDto,@RequestParam(name = "value", defaultValue = "0.0") double value){

        UtilisateurDto utilisateurDto= this.iUtilisateurService
                .chercherUtilisateurParNumeroIdentite(numeroIdentite);
        //Check if Numero Identity existe
        if(utilisateurDto.getNumeroIdentite() != null
                && StringUtils.hasText(utilisateurDto.getNumeroIdentite()) ){
            compteDto.setUtilisateurDto(utilisateurDto);
            compteDto.setStatusCompte(EnumStatusCompte.OUVERT);
            compteDto.setSolde(value);
            this.iCompteService.nouveauCompte(compteDto);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Compte a été bien crée ");
        }else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Numéro identité non existe ");
        }

    } */

    @PostMapping("nouveauCompte")
    @PostAuthorize("hasRole('ROLE_AGENT_GUICHET')")
    public ResponseEntity<String> creerNouveauCompte(@RequestBody CompteCreationDto compteCreationDto){

        UtilisateurDto utilisateurDto= this.iUtilisateurService
                .chercherUtilisateurParNumeroIdentite(compteCreationDto.getNumeroidentite());
        //Check if Numero Identity existe
        if(utilisateurDto.getNumeroIdentite() != null
                && StringUtils.hasText(utilisateurDto.getNumeroIdentite()) ){
            compteCreationDto.setNumeroidentite(compteCreationDto.getNumeroidentite());
            compteCreationDto.setStatusCompte(EnumStatusCompte.OUVERT);
            compteCreationDto.setSolde(0.0);
            // CompteDto compteDto=new CompteDto();
            CompteDto compteDto=  this.customModelMapper.modelMapper().map(compteCreationDto,CompteDto.class);
            compteDto.setUtilisateurDto(utilisateurDto);
            this.iCompteService.nouveauCompte(compteDto);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Compte a été bien crée ");
        }else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Numéro identité non existe ");
        }



    }
    @GetMapping("listComptes")
    @PostAuthorize("hasRole('ROLE_CLIENT')")
    public List<String> getListComptes(@RequestParam (name = "utilisateurId")Integer utilisateurId) {
        log.info("Liste comptes ");
        return this.iCompteService.listeCompteByUtilisateurId(utilisateurId).stream().map(CompteDto::getRib).collect(Collectors.toList());

    }
}
