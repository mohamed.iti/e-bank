package com.formations.ebank.controllers;


import com.formations.ebank.aspect.Loggable;
import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.OperationDto;
import com.formations.ebank.dtos.RibSoldeDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.Operation;
import com.formations.ebank.services.ICompteService;
import com.formations.ebank.services.IOperationService;
import com.formations.ebank.services.IUtilisateurService;
import com.formations.ebank.utils.CustomModelMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor
public class DashboardRestController {

    //List RIB and Solde
    private final IUtilisateurService iUtilisateurService;
    private final ICompteService iCompteService;
    private final CustomModelMapper customModelMapper;
    private final IOperationService iOperationService;

    @Loggable
    @GetMapping("/listRibSolde")
    @PostAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_AGENT_GUICHET')")
    public List<Map<String, Object>> getListRibSolde(
            @RequestParam("utilisateurId") Integer utilisateurId) {


        UtilisateurDto utilisateurDto = this.iUtilisateurService.chercherUtilisateurParId(utilisateurId);
        List<CompteDto> compteDtoList = iCompteService.listeCompteByUtilisateur(utilisateurDto);
       /* Map<String, Double> listeRIBsolde= compteDtoList.stream().
                collect(Collectors.toMap(CompteDto::getRib, CompteDto::getSolde));
       /* for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        } */
        // Créer le HashMap à partir de la liste des CompteDto
        //  Map<String, Double> listeRIBsolde = new HashMap<>();
        List<Map<String, Object>> result = new ArrayList<>();
        for (CompteDto compteDto : compteDtoList) {
            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("rib", compteDto.getRib());
            jsonObject.put("solde", compteDto.getSolde());
            result.add(jsonObject);
        }

        return result;
        /*return compteDtoList.stream()
                .collect(Collectors.toMap(CompteDto::getRib, CompteDto::getSolde)); */
        // return listeRIBsolde;
    }

    /*
        @GetMapping("/listRibSolde")

        List<CompteDto> getListRibSolde( @RequestParam("utilisateurId") Integer utilisateurId) {
            List<CompteDto> compteDtoList = (List<CompteDto>) iCompteService.listeCompteByUtilisateur(utilisateurId);
            return compteDtoList;
        } */
    @GetMapping("/operations")
    @PostAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_AGENT_GUICHET')")
    public List<OperationDto> getOperations(
            @RequestParam(name = "pageNo", defaultValue = "0") int pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
            @RequestParam(name = "sortBy", defaultValue = "dateRealisation") String sortBy,
            @RequestParam(name = "sortDirection", defaultValue = "ASC") String sortDirection,
            @RequestParam(name = "utilisateurId", defaultValue = "utilisateurId") Integer utilisateurId) {
        UtilisateurDto utilisateurDto = iUtilisateurService.chercherUtilisateurParId(utilisateurId);
        Page<OperationDto> operationDtosResult = this.iOperationService.findAllByUtilisateur(utilisateurDto, pageNo, pageSize, sortBy, sortDirection);
        System.out.println("Size page par utilisateur = " + operationDtosResult.getContent().size());

        return operationDtosResult.getContent();

    }
/*
    @GetMapping("/operationsTopTen")
    public List<Map<String, Object>> getOperationsTopten(
            @RequestParam( name = "pageNo", defaultValue = "0") int pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
            @RequestParam(  name = "sortBy",defaultValue = "dateRealisation") String sortBy,
            @RequestParam(name="sortDirection",defaultValue = "ASC") String sortDirection,
            @RequestParam(name="utilisateurId",defaultValue = "utilisateurId") Integer utilisateurId) {
        UtilisateurDto utilisateurDto=iUtilisateurService.chercherUtilisateurParId(utilisateurId);
        CompteDto compteDtoLastUpdated=iCompteService.checherCompteRecementMouvemente(utilisateurDto);
       List <OperationDto> operationDtoLastUpdated= iOperationService.chercherOperationrecementMouvemente(utilisateurDto);
       var RIB=customModelMapper.modelMapper().map(operationDtoLastUpdated,OperationDto.class);
        Page<OperationDto> operationDtosResult = this.iOperationService.
                chercherTopTenOperationParRib(operationDtoLastUpdated.getRib(),
                pageNo, pageSize, sortBy, sortDirection);
        Page<OperationDto> operationDtosResult = this.iOperationService.
                chercherTopTenOperationParRib(RIB.getRib(),
                        pageNo, pageSize, sortBy, sortDirection);
        List<Map<String, Object>> resultOP = new ArrayList<>();
        for (OperationDto operationDto : operationDtoLastUpdated) {
            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("virement_en_votre_faveur", operationDto.getIntituleDto().getNom());
            jsonObject.put("Type_operation", operationDto.getTypeOperation());
            jsonObject.put("Date_operation",operationDto.getDateRealisation());
            jsonObject.put(("Montant_operation"),operationDto.getMontant());
            jsonObject.put(("RIB"),operationDto.getRib());
            resultOP.add(jsonObject);
        }
        System.out.println(  "Size page par utilisateur = " +         operationDtosResult.getContent().size());

        return resultOP;

    } */

    @GetMapping("/operationsTopTen")
    @PostAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_AGENT_GUICHET')")
    public List<Map<String, Object>> getOperationsTopten(
            @RequestParam(name = "pageNo", defaultValue = "0") int pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
            @RequestParam(name = "sortBy", defaultValue = "dateRealisation") String sortBy,
            @RequestParam(name = "sortDirection", defaultValue = "ASC") String sortDirection,
            @RequestParam(name = "utilisateurId", defaultValue = "0") Integer utilisateurId,
            @RequestParam(name = "RIB", defaultValue = "null") String rib) {

        if (!"null".equals(rib) && utilisateurId != 0) {
            // Retrieve all operations by RIB without pagination
            List<OperationDto> operationsByRIB = iOperationService.chercherOperationsParRib(rib, sortBy, sortDirection);
            List<Map<String, Object>> OPRIB = new ArrayList<>();
            for (OperationDto operationDto : operationsByRIB) {
                Map<String, Object> jsonObject = new HashMap<>();
                jsonObject.put("virement_en_votre_faveur", operationDto.getIntituleDto().getNom());
                jsonObject.put("Type_operation", operationDto.getTypeOperation());
                jsonObject.put("Date_operation", operationDto.getDateRealisation());
                jsonObject.put("Montant_operation", operationDto.getMontant());
                jsonObject.put("RIB", operationDto.getRib());
                OPRIB.add(jsonObject);
            }
            System.out.println("Size page par operation = " + operationsByRIB.size());
            return OPRIB;
        } else {
            UtilisateurDto utilisateurDto = iUtilisateurService.chercherUtilisateurParId(utilisateurId);
            CompteDto compteDtoLastUpdated = iCompteService.checherCompteRecementMouvemente(utilisateurDto);
            Page<OperationDto> operationDtosResult = iOperationService.chercherTopTenOperationParRib(
                    compteDtoLastUpdated.getRib(), pageNo, pageSize, sortBy, sortDirection);

// Limiter les résultats à 10 opérations maximum
            List<OperationDto> limitedOperationDtos = operationDtosResult.getContent().subList(0, Math.min(10, operationDtosResult.getContent().size()));

            List<Map<String, Object>> resultOP = new ArrayList<>();
            for (OperationDto operationDto : limitedOperationDtos) {
                Map<String, Object> jsonObject = new HashMap<>();
                jsonObject.put("virement_en_votre_faveur", operationDto.getIntituleDto().getNom());
                jsonObject.put("Type_operation", operationDto.getTypeOperation());
                jsonObject.put("Date_operation", operationDto.getDateRealisation());
                jsonObject.put("Montant_operation", operationDto.getMontant());
                jsonObject.put("RIB", operationDto.getRib());
                resultOP.add(jsonObject);
            }

            System.out.println("Size page par utilisateur = " + limitedOperationDtos.size());
            return resultOP;
        }
    }
}

