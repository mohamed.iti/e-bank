package com.formations.ebank.controllers;


import com.formations.ebank.dtos.OperationVirementDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.enums.EnumTypeOperation;
import com.formations.ebank.services.IOperationService;
import com.formations.ebank.services.IUtilisateurService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor
public class OperationRestController {

    private final IOperationService iOperationService;
    private final  IUtilisateurService iUtilisateurService;
    @PostMapping("/operation")
    @PostAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<String> operation(@RequestBody OperationVirementDto operationVirementDto) throws Exception{

       UtilisateurDto utilisateurDtoCompteOperationCrediteur =
               iUtilisateurService.chercherUtilisateurParId(operationVirementDto.getUtilisateurCrediteurId());

         iOperationService.nouveauOperation(operationVirementDto.getRibDestinateur(),
                operationVirementDto.getRibCrediteur(),operationVirementDto.getMontant(),
                operationVirementDto.getTypeOperation(),
                utilisateurDtoCompteOperationCrediteur,
                "Virement en faveur de "+utilisateurDtoCompteOperationCrediteur.getNom()
        );
        return ResponseEntity.status(HttpStatus.OK).body( operationVirementDto.toString()+"Test operation ok");
    }
}
