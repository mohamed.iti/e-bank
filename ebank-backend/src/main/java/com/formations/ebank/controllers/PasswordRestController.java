package com.formations.ebank.controllers;

import com.formations.ebank.dtos.ChangePasswordDTO;
import com.formations.ebank.dtos.CompteCreationDto;
import com.formations.ebank.dtos.CompteDto;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.enums.EnumStatusCompte;
import com.formations.ebank.services.IUtilisateurService;
import com.formations.ebank.utils.CustomModelMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor
public class PasswordRestController {
    private final IUtilisateurService iUtilisateurService;
    private final CustomModelMapper customModelMapper;
   /* @PutMapping("changerpassword")
    public ResponseEntity<String> changeUtilisateur(@RequestBody ChangePasswordDTO changePasswordDTO){

        UtilisateurDto utilisateurDto= this.iUtilisateurService
                .chercherUtilisateurParNumeroIdentite("numeroIdentitetest");
        //Check if Numero Identity existe
        if(utilisateurDto.getNumeroIdentite()!= null
                && StringUtils.hasText(utilisateurDto.getNumeroIdentite()) ){
            changePasswordDTO.setPassword(changePasswordDTO.getPassword());
            // CompteDto compteDto=new CompteDto();
            UtilisateurDto utilisateur=  this.customModelMapper.modelMapper().map(changePasswordDTO,UtilisateurDto.class);
            utilisateur.setPassword(utilisateurDto.getPassword());
            this.iUtilisateurService.changePassword(utilisateur);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("password changé ");
        }else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Numéro identité non existe ");
        }

    } */

    @PutMapping("changerpassword")
    @PostAuthorize("hasRole('ROLE_CLIENT') or hasRole('ROLE_AGENT_GUICHET')")
    public ResponseEntity<UtilisateurDto> updateCustomer(@RequestBody @Valid ChangePasswordDTO changePasswordDTO) {
        return new ResponseEntity<>(iUtilisateurService.changePasswordByPasswordEncoder(changePasswordDTO), HttpStatus.OK);
    }
}
