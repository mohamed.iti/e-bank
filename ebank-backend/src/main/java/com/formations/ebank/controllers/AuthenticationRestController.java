package com.formations.ebank.controllers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200/") // sur mesure
@RestController
@RequestMapping("ebank/")
@Slf4j
@RequiredArgsConstructor
public class AuthenticationRestController {
private final AuthenticationManager authenticationManager;


    @PostMapping("login")
    public void login() {

        System.out.println("Login controller");
    }
}
