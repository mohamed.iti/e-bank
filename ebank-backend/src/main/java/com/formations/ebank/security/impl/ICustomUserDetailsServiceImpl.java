package com.formations.ebank.security.impl;


import com.formations.ebank.entities.CustomUserDetailsSecurity;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.repositories.UtilisateurJpaRepo;
import com.formations.ebank.utils.CustomModelMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class ICustomUserDetailsServiceImpl implements UserDetailsService {

    private final UtilisateurJpaRepo utilisateurJpaRepo;
    private final CustomModelMapper customModelMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur utilisateur;
        try {
            Optional<Utilisateur> optionalUtilisateur = this.utilisateurJpaRepo.findByLogin(username);
            return optionalUtilisateur.map(CustomUserDetailsSecurity::new).orElse(null);
        } catch (Exception exception) {
            return null;
        }
    }
}
