package com.formations.ebank.security;

public interface IMailSenderService {

     void sendNewMail(String to, String subject, String body);
}