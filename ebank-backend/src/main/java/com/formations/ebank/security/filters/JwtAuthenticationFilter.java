package com.formations.ebank.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.formations.ebank.dtos.UtilisateurDto;
import com.formations.ebank.entities.CustomUserDetailsSecurity;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {


    private final AuthenticationManager authenticationManager;
    //intercept login and password communicate with UserDetailService  to authenticate user in application

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        System.out.println("attemptAuthentication method ");


        String login;
        String password;
        ObjectMapper mapper = new ObjectMapper();
        try {
            UtilisateurDto utilisateurDtoToAuthenticated = mapper.readValue(request.getInputStream(),UtilisateurDto.class);
            System.out.println("Login from UtilisateurDto dto = "+utilisateurDtoToAuthenticated.getLogin());
            login=utilisateurDtoToAuthenticated.getLogin();
            password=utilisateurDtoToAuthenticated.getPassword();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Login  : "+login);
        System.out.println("Password  : "+password);


       // response.setHeader("error-message","Login ou password incorecct");
/*
       try {
           UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=
                   new UsernamePasswordAuthenticationToken(login,password);
         //  response.reset();
//to remove all attributes header
           response.setHeader("success","login ok  message");

               return authenticationManager.authenticate(usernamePasswordAuthenticationToken);
       }catch (Exception e){

           // response.reset(); //to remove all attributes header
               response.setHeader("error-login","Login ou mot de passe erronés ");

               return null;

       }
       */
       /* UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=
                new UsernamePasswordAuthenticationToken(login,password);
        return authenticationManager.authenticate(usernamePasswordAuthenticationToken);*/
        try {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=
                    new UsernamePasswordAuthenticationToken(login,password);
            return authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        }catch (Exception exception){

            return null;
        }
  }


    //When authentication success create a token , then choose source to save it (Generated token)

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        System.out.println("successfulAuthentication method ");

        //get authenticated use
        CustomUserDetailsSecurity customUserApp=(CustomUserDetailsSecurity)authResult.getPrincipal(); //get connected or authenticated successfully user
        System.out.println("User app = "+customUserApp);
        //generate token
        //Header  : Algo type jwt
        //Payload  : claims , username, issuer, audience expiation, roles
        //Signature  : algorithm sign key

        Algorithm algorithmHmac256=Algorithm.HMAC256("SecretKey256");
        String jwtAccessToken= "Bearer "+JWT.create()
                .withSubject(customUserApp.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+60* 60 * 1000))// 2 minutes
                        .withClaim("roles",customUserApp
                                .getAuthorities().stream().map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList())).sign(algorithmHmac256);
        //response.setHeader("Authorization",jwtAccessToken);
       // System.out.println("response.Header  : "+response.getHeader("Authorization"));

        //Refresh token   , just to generate access token revoke token, without roles
        String jwtRefreshToken= JWT.create()
                .withSubject(customUserApp.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+1*60*1000)) //15 minutes
                .sign(algorithmHmac256);
       // response.setHeader("Authorization",jwtRefreshToken);
        //System.out.println("response.Header  : "+response.getHeader("Authorization"));
        Map<String,String>mapTokens=new HashMap<>();
        mapTokens.put("access-token",jwtAccessToken);
        mapTokens.put("refresh-token",jwtRefreshToken);
        //convert to json by spring
        response.setContentType("application/json");

new ObjectMapper().writeValue(response.getOutputStream(),mapTokens);

        //  super.successfulAuthentication(request, response, chain, authResult);
    }

}

