package com.formations.ebank.security.filters;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthorizationFilter extends OncePerRequestFilter {
    // need to check role and authorities by order filter
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        System.out.println("Start method JwtAuthorizationFilter dofilter");
        System.out.println("        request.getHeader(Authorization)"+request.getHeader("Authorization"));

        //String authorizationAccessTokenHeader=request.getParameter("Authorization");
        String authorizationAccessTokenHeader= request.getHeader("Authorization");
        System.out.println("authorizationAccessTokenHeader "+authorizationAccessTokenHeader);
        if (authorizationAccessTokenHeader!=null && authorizationAccessTokenHeader.startsWith("Bearer ")) {
            try {
                System.out.println("Authorization access-token : "+authorizationAccessTokenHeader);

                //Extract 7 first cherters as "Bearer "
                String jwtAccessToken=authorizationAccessTokenHeader.substring(7);
                Algorithm algorithmHmac256=Algorithm.HMAC256("SecretKey256");
                JWTVerifier jwtVerifier= JWT.require(algorithmHmac256).build();
                DecodedJWT decodedJWT= jwtVerifier.verify(jwtAccessToken);//Contains information about user
                String login=decodedJWT.getSubject();
                //Check roles with get Session , Role, User
                String[] roles =decodedJWT.getClaim("roles").asArray(String.class);

                Collection<GrantedAuthority> authorities= Arrays.stream(roles)
                        .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
                //No need password , not put on Token Jwt
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=
                        new UsernamePasswordAuthenticationToken(login,null,authorities);
                //Authenticate a user into context as Session
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//Pass to next filter as forward request
                filterChain.doFilter(request,response);
            }catch (Exception ee){
                System.out.println(ee);
                response.setHeader("error-message",ee.getMessage());
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else {
            //Can pass because resource is already authenticated
            //Pass to next filter as forward request
            filterChain.doFilter(request,response);
        }

    }
}
