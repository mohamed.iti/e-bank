package com.formations.ebank.repositories;

import com.formations.ebank.entities.Compte;
import com.formations.ebank.entities.Operation;
import com.formations.ebank.entities.Utilisateur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OperationJpaRepo  extends JpaRepository<Operation,Integer> {
    @Override
    Optional<Operation> findById(Integer integer);
    Page<Operation> findAllByUtilisateur(Utilisateur utilisateur, Pageable pageable);
    Page<Operation> findAllByRib(String rib, Pageable pageable);
    List<Operation> findByUtilisateur(Utilisateur utilisateur);

}
