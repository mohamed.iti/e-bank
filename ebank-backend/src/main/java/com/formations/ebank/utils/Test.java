package com.formations.ebank.utils;


import com.formations.ebank.dtos.*;
import com.formations.ebank.entities.*;
import com.formations.ebank.enums.EnumStatusCompte;
import com.formations.ebank.enums.EnumTypeOperation;
import com.formations.ebank.enums.EnumTypeProfile;
import com.formations.ebank.repositories.*;
import com.formations.ebank.security.SecurityManagerService;
import com.formations.ebank.services.ICompteService;
import com.formations.ebank.services.IOperationService;
import com.formations.ebank.services.IUtilisateurService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

@SpringBootApplication
@EnableAspectJAutoProxy
@Configuration
@Slf4j
@RequiredArgsConstructor
public class Test {

    private final IUtilisateurService iUtilisateurService;
    private final ProfileJpaRepo profileJpaRepo;
    private final CustomModelMapper customModelMapper;
    private final UtilisateurJpaRepo utilisateurJpaRepo;
    private final CompteJpaRepo compteJpaRepo;
    private final ICompteService iCompteService;
    private final OperationJpaRepo operationJpaRepo;
    private final UtilisateurProfileJpaRepo utilisateurProfileJpaRepo;
    private final SecurityManagerService securityManagerService;
    private final IOperationService iOperationService;

    private final PasswordEncoder passwordEncoder;
    @Bean
    CommandLineRunner commandLineRunner() {


        return args -> {

           /* UtilisateurDto utilisateurDto= this.iUtilisateurService.chercherUtilisateurParId(1);

            List<CompteDto> compteDtoList=iCompteService.listeCompteByUtilisateur(utilisateurDto);
            compteDtoList.forEach(
                    compteDto -> {
                        System.out.println(compteDto.getRib()) ;
                        System.out.println(compteDto.getSolde()) ;

                    }
            );
*/
                /*  Utilisateur utilisateur= this.utilisateurJpaRepo.findById(1).get();
            Page<Operation> operationDtos=operationJpaRepo.findAllByUtilisateur(utilisateur,null);
            System.out.println(  "Size page par utilisateur =" +         operationDtos.getContent().size());*/

            /*UtilisateurDto utilisateurDto= this.iUtilisateurService.chercherUtilisateurParId(2);
           Page<OperationDto> operationDtos=iOperationService.
                   findAllByUtilisateur(utilisateurDto,1,10,"dateRealisation","ASC");
            System.out.println(  "Size page par utilisateur = " +         operationDtos.getContent().size());*/
            /*Optional<Compte> compteBoLastUpdated=compteJpaRepo.findAll().stream()
                    .max(Comparator.comparing(Compte::getDerniereDateModification));
            System.out.println(compteBoLastUpdated.get().getDerniereDateModification());*/
        /*   UtilisateurDto utilisateurDto= this.iUtilisateurService.chercherUtilisateurParId(1);

            List<CompteDto>compteDtoListParUtilisateur=iCompteService.listeCompteByUtilisateur(utilisateurDto);
           compteDtoListParUtilisateur.forEach(compteDto -> System.out.println(compteDto.getRib()));

             CompteDto compteDtoLastUpdated=iCompteService
                     .checherCompteRecementMouvemente(utilisateurDto);

            System.out.println(compteDtoLastUpdated.getDerniereDateModification()); */
           /* Page<Operation>operationListBoParRib=operationJpaRepo.findAllByRib("42775643241711155770921", null);
            System.out.println(  "Operation size par RIB = " +         operationListBoParRib.getContent().size());*/
                //Page<OperationDto>operationDtoPage=operationListBoParRib.map(operationListBoParRib,OperationDto.class);
            /* Page<OperationDto> operationDtos=iOperationService.
                    chercherTopTenOperationParRib("42775643241711155770921",
                            1,5,"dateRealisation","ASC");
            System.out.println(  "Operation size par RIB = " +         operationDtos.getContent().size());*/

          /*  UtilisateurDto utilisateurDtoByIdentiteNumero=iUtilisateurService.chercherUtilisateurParNumeroIdentite("testAngular1");
            System.out.println("utilisateurDtoByIdentiteNumero : "+utilisateurDtoByIdentiteNumero.getNom());*/

           /* String newPassword="newpasswordTest2";
            String identiteNumero="BH330949";
            ChangePasswordDTO changePasswordDTO=new ChangePasswordDTO();
            changePasswordDTO.setPassword(newPassword);
            changePasswordDTO.setNumeroIdentite(identiteNumero);
            UtilisateurDto utilisateurDtoChangePass=iUtilisateurService.changePassword(changePasswordDTO);

            System.out.println("Change password test : "+utilisateurDtoChangePass.getPassword());*/


            //Spring security test
           /* Role roleClient=Role.builder().id(1).name("ROLE_CLIENT").build();
            Role roleADMIN=Role.builder().id(2).name("ROLE_ADMIN").build();
            Role roleAgentGuichet=Role.builder().id(3).name("ROLE_AGENT_GUICHET").build();
            iRoleJpaRepo.saveAndFlush(roleClient);
            iRoleJpaRepo.saveAndFlush(roleADMIN);

            iRoleJpaRepo.saveAndFlush(roleAgentGuichet);*/
      /*      Role roleAdmin=iRoleJpaRepo.findById(2).get();
		List<Role>roles=new ArrayList<>();
		roles.add(roleAdmin);
		User userAdmin=User.builder().id(null).authorities(roles).
				password(passwordEncoder.encode("admin")).username("admin").build();
		iUserJpaRepo.save(userAdmin);
*/
		/*Role roleClient=iRoleJpaRepo.findById(1).get();
		List<Role>roles2=new ArrayList<>();
            roles2.add(roleClient);
		User userClient=User.builder().id(null).authorities(roles2).
				password(passwordEncoder.encode("client")).username("client").build();
		iUserJpaRepo.save(userClient);*/

            //System.out.println("Role = "+roleAdmin.getName());
		/*User userClientSearch=iUserJpaRepo.findByUsername("client").get();
		System.out.println("Client = "+userClientSearch.getAuthorities().size());
		List<Role>authorities=userClientSearch.getAuthorities();
		for (Role  role:authorities)
			System.out.println(role.getName());*/

            //Change password
               /*String newPassword="client";
            String identiteNumero="BH330949";
            ChangePasswordDTO changePasswordDTO=new ChangePasswordDTO();
            changePasswordDTO.setPassword(newPassword);
            changePasswordDTO.setNumeroIdentite(identiteNumero);
            UtilisateurDto utilisateurDtoChangePass=iUtilisateurService.changePasswordByPasswordEncoder(changePasswordDTO);

            System.out.println("Change password test : "+utilisateurDtoChangePass.getPassword());*/
           /* List<Compte> compteList=compteJpaRepo.findByUtilisateurId(1);
            System.out.println(compteList.size());*/
              /*List<CompteDto> compteDtoList=iCompteService.listeCompteByUtilisateurId(1);
            System.out.println(compteDtoList.size()); */
            // change password for imaneuser
                      String newPassword="admin";
            String identiteNumero="15800025";
            ChangePasswordDTO changePasswordDTO=new ChangePasswordDTO();
            changePasswordDTO.setPassword(newPassword);
            changePasswordDTO.setNumeroIdentite(identiteNumero);
            UtilisateurDto utilisateurDtoChangePass=iUtilisateurService.changePasswordByPasswordEncoder(changePasswordDTO);

            System.out.println("Change password test : "+utilisateurDtoChangePass.getPassword());
        };
    }
}
