package com.formations.ebank.dtos;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.*;

import java.io.Serializable;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurProfileKeyDto implements Serializable {

@ToString.Exclude
     private Integer utilisateurId;
@ToString.Exclude
     private Integer profileId;


}
