package com.formations.ebank.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UtilisateurDto implements Serializable {


    private Integer id;
    private String nom;
    private String prenom;
    private String login;
    private String password;
    private String passwordValidation;
    private Date dateAnniversaire;
    private String adresseMail;
    private String adressePostale;
    private String numeroIdentite;
    private List<CompteDto> compteDtoLists =Collections.emptyList();
    @JsonIgnore
    private List<OperationDto> operationDtoList= Collections.emptyList();
    private List<UtilisateurProfileDto> ListUtilisateurProfileDto=Collections.emptyList();

}
