package com.formations.ebank.dtos;

import com.formations.ebank.enums.EnumStatusCompte;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompteDto implements Serializable {

    private Integer id;
    private EnumStatusCompte statusCompte;
    private  Double solde;
    private  String rib;
    private UtilisateurDto utilisateurDto;
    private Date derniereDateModification;

}
