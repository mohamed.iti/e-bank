package com.formations.ebank.dtos;


import com.formations.ebank.enums.EnumTypeOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OperationVirementDto {
    private String ribDestinateur;
    private String ribCrediteur;
    private Integer utilisateurCrediteurId;
    private EnumTypeOperation typeOperation;
    private Double montant;
    private Date dateRealisation;


}
