package com.formations.ebank.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RibSoldeDto {

    private Integer utilisateurId;
    private HashMap<String,Double> listRibSolde=new HashMap<>();

}
