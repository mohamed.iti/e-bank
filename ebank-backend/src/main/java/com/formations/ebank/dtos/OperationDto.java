package com.formations.ebank.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.formations.ebank.entities.Utilisateur;
import com.formations.ebank.enums.EnumTypeOperation;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.io.Serializable;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OperationDto implements Serializable {


    private Integer id;
    private EnumTypeOperation typeOperation;
    private UtilisateurDto intituleDto;
    private Double montant;
    private Date dateRealisation;
    private String rib;



}
