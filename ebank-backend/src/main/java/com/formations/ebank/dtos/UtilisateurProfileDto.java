package com.formations.ebank.dtos;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.formations.ebank.entities.UtilisateurProfileKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurProfileDto implements Serializable {


    private Date crerationDate;
    private UtilisateurProfileKey utilisateurProfileKey;

    @JsonIgnore
    private UtilisateurDto utilisateur;
    @JsonIgnore
    private ProfileDto profile;


}
