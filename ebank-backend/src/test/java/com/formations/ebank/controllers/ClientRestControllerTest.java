package com.formations.ebank.controllers;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ClientRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtLml0aTAwOCIsImV4cCI6MTcyMDE4MjQ4Nywicm9sZXMiOlsiUk9MRV9BR0VOVF9HVUlDSEVUIl19.x2SxVQqRoS_eCfFR9whru7kqF7tIfwHpfHnBEeQym7M";

    private String baseLoginUrl="http://localhost:8000/ebank/login";

    @BeforeEach
    void setUp() throws Exception {
        System.out.println("init test");
     /*  ResultActions resultActions= this.mockMvc.perform(post(this.baseLoginUrl)
               .param("username","miti004").param("password","admin"));
        MvcResult mvcResult= resultActions.andDo(print()).andReturn();
        String contentString=mvcResult.getResponse().getContentAsString();
        JSONObject jsonObject=new JSONObject(contentString);
        token="Bearer "+jsonObject.getJSONObject("data").getString("token");*/

    }
    @Test
    void getListClientsDao() throws Exception {
/*
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

        params.add("username", "miti004");
        params.add("password", "admin");

        ResultActions result
                = mockMvc.perform(post(baseLoginUrl)
                        .params(params)
                        .with(httpBasic("miti004","admin"))
                        .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        token= jsonParser.parseMap(resultString).get("access_token").toString();
        System.out.println("token" +token);
*/

        assertNotNull(token);
        mockMvc.perform(MockMvcRequestBuilders.get("/ebank/listClients")
                .header("Authorization", token)).andExpect(status().isOk());
    }
}